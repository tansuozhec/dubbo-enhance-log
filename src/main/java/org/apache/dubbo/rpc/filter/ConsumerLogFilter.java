/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.dubbo.rpc.filter;

import com.google.gson.Gson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.dubbo.common.logger.Logger;
import org.apache.dubbo.common.logger.LoggerFactory;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * Output consumer call log
 *
 * @author tansuozhec
 * @version 1.0.0
 * @create 2021-04-01
 */
@Activate(group = CommonConstants.CONSUMER)
public class ConsumerLogFilter implements Filter {

    private static final String MESSAGE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    private static final Gson gson = new Gson();

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        long start = System.currentTimeMillis();
        RpcContext context = RpcContext.getContext();
        String remoteAddress = context.getRemoteAddressString();
        String localAddress = context.getLocalAddressString();
        try {
            AccessLog accessReq = new AccessLog();
            accessReq.setInterfaceName(invocation.getInvoker().getInterface().getName());
            accessReq.setMethodName(invocation.getMethodName());
            accessReq.setArgs(invocation.getArguments());
            accessReq.setRemoteIp(remoteAddress);
            accessReq.setLocalIp(localAddress);
            StringBuffer sn_in = new StringBuffer();
            sn_in.append(LocalDateTime.now().format(DateTimeFormatter.ofPattern(MESSAGE_DATE_FORMAT)));
            sn_in.append(" ");
            sn_in.append(uuid);
            sn_in.append(" dubbo consumer ");
            sn_in.append(localAddress);
            sn_in.append("-->");
            sn_in.append(remoteAddress);
            sn_in.append(" ");
            sn_in.append(accessReq.getInterfaceName());
            sn_in.append(" ");
            sn_in.append(accessReq.getMethodName());
            sn_in.append(" request:");
            sn_in.append(gson.toJson(accessReq.getArgs()));
            logger.info(sn_in.toString());
            Result result = invoker.invoke(invocation);
            AccessLog accessRsp = new AccessLog();
            accessRsp.setInterfaceName(invocation.getInvoker().getInterface().getName());
            accessRsp.setMethodName(invocation.getMethodName());
            accessRsp.setResponse(result.getValue());
            accessRsp.setRemoteIp(remoteAddress);
            accessRsp.setLocalIp(localAddress);
            accessRsp.setElapsedTime(System.currentTimeMillis() - start);
            StringBuffer sn_out = new StringBuffer();
            sn_out.append(LocalDateTime.now().format(DateTimeFormatter.ofPattern(MESSAGE_DATE_FORMAT)));
            sn_out.append(" ");
            sn_out.append(uuid);
            sn_out.append(" dubbo consumer ");
            sn_out.append(localAddress);
            sn_out.append("-->");
            sn_out.append(remoteAddress);
            sn_out.append(" ");
            sn_out.append(accessRsp.getInterfaceName());
            sn_out.append(" ");
            sn_out.append(accessRsp.getMethodName());
            sn_out.append(" ");
            sn_out.append(accessRsp.getElapsedTime());
            sn_out.append("ms response:");
            sn_out.append(gson.toJson(accessRsp.getResponse()));
            logger.info(sn_out.toString());
            return result;
        } catch (Exception e) {
            logger.error(uuid + " dubbo consumer request error:", e);
            throw e;
        }
    }


}
