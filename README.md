# Dubbo调用日志记录工具

## 介绍
```
这是一个Dubbo日志增强工具，基于Dubbo SPI插件机制实现.
Dubbo原生日志过滤器只能输出服务端的调用记录，并且没有请求参数和返回结果.
现提供该工具，可输出PROVIDER生产者的被调用日志，CONSUMER消费者的调用日志，日志包含<调用者IP，服务者IP，调用接口，调用方法，消耗时间，请求参数，返回结果>.
现在支持Dubbo3.0版本和Dubbo2.7版本
```

## 使用
#### 直接Maven配置文件pom中引用该包即可，无需多余配置，开箱即用
```
Dubbo3.0及以上版本引用
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <version>3.0.0-SNAPSHOT</version>
    <artifactId>dubbo-enhance-log</artifactId>
</dependency>
```
```
Dubbo2.7及以上版本引用
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <version>2.7.0-SNAPSHOT</version>
    <artifactId>dubbo-enhance-log</artifactId>
</dependency>
```
## 示例
```
生产者日志
--------时间------------ -|------------uuid----------------|---消费者标识--|-----本地ip--|-指向-|------远程ip--------|---------------接口-----------|--方法--|-耗时-|--请求&返回---
: 2021-12-06 10:30:33.898 50511e95768343d98385909f15c99b56 dubbo consumer 192.168.43.28:0-->192.168.43.28:20880 org.apache.dubbo.demo.DemoService sayHello request:["dubbo"]
: 2021-12-06 10:30:34.250 50511e95768343d98385909f15c99b56 dubbo consumer 192.168.43.28:0-->192.168.43.28:20880 org.apache.dubbo.demo.DemoService sayHello 361ms response:"Hello dubbo"


消费者日志
--------时间------------ -|------------uuid----------------|---生产者标识--|-------本地ip----|-指向-|-------远程ip--- --|---------------接口-----------|--方法--|-耗时-|--请求&返回---
: 2021-12-06 10:30:34.193 d8527ecf4d9845ff9a1781bb6ec02deb dubbo provider 192.168.43.28:20880<--192.168.43.28:60570 org.apache.dubbo.demo.DemoService sayHello request:["dubbo"]
: 2021-12-06 10:30:34.223 d8527ecf4d9845ff9a1781bb6ec02deb dubbo provider 192.168.43.28:20880<--192.168.43.28:60570 org.apache.dubbo.demo.DemoService sayHello 33ms response:"Hello dubbo"

```

